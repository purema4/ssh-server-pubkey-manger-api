export default class Command {
    constructor(command) {
        this.command = command
    }

    spawnCommmand() {
        return new Promise((resolve,reject)=> {
            let output = '';

            const comm = proc.spawn('sh', ['-c', this.command])

            comm.stdout.on('data', d => {
                output += d
            })

            comm.on('close', code => {
                resolve(output, code)
            })

            comm.on('error', err => {
                reject(err)
            })
        })
    }
}