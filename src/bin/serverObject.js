export default class serverObject {
  constructor(hostname, ip=null, port=22) {
      this.hostname = hostname
      this.ip = ip
      this.port = port
  }
}