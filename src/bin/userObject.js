import fetch from 'node-fetch'

export default class userObject {
    constructor (name='', keys=[]) {
        this.name = name;
        this.keys = keys
    }


    async addKeysFromGithub() {
      
        const response = await fetch(`https://api.github.com/users/${this.name}/keys`)
        const jsonKeys = await response.json()
        
        jsonKeys.map(index => {
            this.keys.push(index.key)
        })

        return this.keys
    }
}