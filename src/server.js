'use strict';

import http from 'http'
import express from 'express'
import bodyParser from 'body-parser'

//import util from 'util'


import sshLiaison from './bin/sshLiaison'
import userObject from './bin/userObject'

let ssh = new sshLiaison();

const app = express();


app.use(bodyParser.urlencoded({ extended: false }))

app.get('/', (req,res) => {
  res.setHeader('Content-Type','text/plain')
  let purema4 = new userObject('purema4')
  purema4.addKeysFromGithub()
  .then(data => {
      res.setHeader('Content-Type','text/plain')
      res.status(200).send(JSON.stringify(data))
  })
  .catch(err => {
    res.status(503).send(err)
  })
})
 
// parse application/json 
app.use(bodyParser.json())

app.listen(3000, _ => {
    console.log('listening on port 3000')
})